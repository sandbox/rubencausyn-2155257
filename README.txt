-- SUMMARY --

Adds postscribe to your site.


-- REQUIREMENTS --

* None.


-- INSTALLATION --



1. download https://github.com/krux/postscribe library under sites/all/libraries (or sites/sitename/libraries) and ,name the folder postscribe

2. enable the module

-- CONTACT --
email: ruben@logicinmotion.be
